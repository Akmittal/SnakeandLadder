from snake import Snake
from ladder import Ladder
class Board:
	positions  = range(100)
	def __init__(self):
		self.length = 10
		self.snake = []
		self.snakedict={}
		self.ladderdict={}
		self.ladder = [1,2,3,4,5]
		for i in range(5):
			sn = Snake()
			self.snakedict[sn.getHead()]=sn.getTail()
			self.snake.append(sn)
			ld = Ladder()
			self.ladderdict[ld.getBottom()]=ld.getTop()
	def isSnake(self,no):
		if no in self.snakedict.keys():
			return True
		else:
			return False
	def isLadder(self,no):
		if no in self.ladderdict.keys():
			return True
		else:
			return False
	def escalate(self,no):
		return self.ladderdict.get(no)
	def deescalate(self,no):
		return self.snakedict.get(no)
	
		

