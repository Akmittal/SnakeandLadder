from numbergenerator import numbergenerator
class Snake:
	def __init__(self):
		nm=numbergenerator()
		self.head=nm.genBigNumber()
		self.tail=nm.genSmallNumber()
	def getHead(self):
		return self.head
	def getTail(self):
		return self.tail