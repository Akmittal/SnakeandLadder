from board import Board
from player import Player
from dice import Dice

board = Board()
dice = Dice()
while True:
	try:
		no_of_players=int(input("Enter number of players"))
	except ValueError:
		print("Not a valid number")
		continue
	else:
		break

player=[]
for i in range(no_of_players):
	plr=Player()
	name=input("Set name for player {}".format(i))
	plr.setName(name)
	player.append(plr)
def isWon():
	flag=False
	for i in player:
		if i.getCurrentPos()>=100:
			flag=True
			break
	return flag
def wait():
	print("Press Enter to Roll the Dice")
	input()

while(not isWon()):
	for i in range(no_of_players):
		print("\nPlayer {}'s turn".format(player[i].getName()))
		print("current position: {}".format(player[i].getCurrentPos()))
		no=dice.rollDice()
		wait()
		print("Rolling Dice.... number is {}".format(no))
		player[i].move(no)
		if board.isSnake(player[i].getCurrentPos()):
			print("oops!! bit by snake")
			player[i].setCurrent(board.deescalate(player[i].getCurrentPos()))
		if board.isLadder(player[i].getCurrentPos()):
			print("Got a ladder")
			player[i].setCurrent(board.escalate(player[i].getCurrentPos()))
		if isWon():
			print("\nCongrats\nplayer {} won ".format(player[i].getName()))
			print("New Position is: {}".format(player[i].getCurrentPos()))
			break
