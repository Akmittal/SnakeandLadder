from numbergenerator import numbergenerator
class Ladder:
	def __init__(self,top=0,bottom=0):
		nm=numbergenerator()
		self.top=nm.genBigNumber()
		self.bottom=nm.genSmallNumber()
	def getTop(self):
		return self.top
	def getBottom(self):
		return self.bottom
	